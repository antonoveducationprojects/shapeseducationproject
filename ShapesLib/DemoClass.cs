﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesLib;
public class DemoClass
{
    public DemoClass()
    {
    }

    public override string ToString()
    {
            return "I am object of demo class that defined in ShapesLib project";
    }
}